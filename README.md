# Home Assistant - ESPHome automation

A project to dynamically run ESPHome Lint/Build/Deploy tasks.

Unless otherwise specified in the file this project is covered by the MIT License.

This works for me so far.

# How this works / what you need to do if you want to copy this

## Requirements

- Knowledge of ESPHome configuration/templating
- A [GitLab Runner](https://docs.gitlab.com/runner/) on the same network as your ESPHome devices set to run jobs tagged with `ha-esphome-deploy` as we use ESPHome's OTA update function to deploy.
  - Additionally you will need somewhere to run jobs tagged with `ha-esphome-lint` and `ha-esphome-build`, or you can modify `.gitlab-ci/generate-downstream.sh` to remove the tags on the lint/build jobs.
- ESPHome devices already configured and connected to your network.

## Things to check/change/configure

- `.gitlab-ci/default.yaml` contains a `default:` section which specifies the image used for all jobs in the project. You can change the version of ESPHome here by changing the tag for the image.
- If you are using runners without `if-not-present` enabled then you can remove the `pull_policy` line (this is NOT enabled by default).
- You will need to create a CI/CD variable in your project under `Settings>CI/CD>Variables`. I use `SECRETS_YAML` but if you pick something else you will need to modify the `before_script` section to the new variable. The `${VARIABLE_NAME}` formatting is critical to ensure that the new lines remain once the file is written.
- Place your individual device configuration files in `devices/` using `somename.yaml` for each. To avoid issues I would advise keeping to alpha numeric characters
- Everything else is up to you really as long as you use relative paths to include things. I've put everything else in `templates/`.
- Any directory that contains a file using `!secrets` will need a `secrets.yaml` that contains a single line to include the `devices/secrets.yaml` that we write in the `before_script` from `.gitlab-ci/default.yml`

## How this works

This project takes advantage of [dynamic child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines) to create a series of jobs based on the contents of the `devices` directory.

CI will only run if:
- Forced via [run pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) AND `FORCE_CI` is set to `true`.
- On any merge to the default branch (`main` unless changed) or any merge request if files have changed in (including any sub directories/files):
   - `devices/`.
   - `templates/`.
- The `deploy` stages will only run on the projects default branch.

When the CI runs:
- `build-downstream` runs `.gitlab-ci/generate-downstream.sh`.
   - This script collects a list of `yaml` files from `devices/` then creates `downstream-pipelines.yml`.
      - Lines 13-22 create a static start of the file with the `stages` defined and including `.gitlab-ci/default.yml`.
      - Lines 31 onwards happen inside a loop with `$NAME` as a variable containing each of the filenames from `devices/` in turn (without the `.yaml`).
    - `downstream-pipelines.yml` is uploaded as an artifact with a 1 week expiry time
- `run-downstream` triggers a downstream/child pipeline based on the contents of `downstream-pipelines.yml`
- Three stages happen in the downstream pipelines. If the pipeline was forced with `FORCE_CI` OR a file changed in `templates/` then all devices are processed, otherwise only devices that changed will be processed.
   - A unique job is created for each device in each stage.
   - `stage: lint` - Runs `esphome config` against the device configuration to check for errors and blocks the pipeline if there are any.
   - `stage: build` - Runs `esphome compile` against the device configuration and uploads the resulting `firmware.bin` with a 1 week expiry time. A failure here also blocks the pipeline.
   - `stage: deploy` - Downloads the `firmware.bin` from the previous step and runs `esphome upload` to provision the firmware with ESPHomes OTA update mechansim.
      - This step is manual so you can cherry pick which devices are flashed and to prevent the upload happening at an inconvenient time.
