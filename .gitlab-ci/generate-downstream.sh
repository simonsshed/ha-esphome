#!/bin/bash

# Check if there are any `yaml` files in `devices/`
shopt -s nullglob dotglob
GLOBIGNORE='devices/secrets.yaml'
files=(devices/*.yaml)
if [ ${#files[@]} -eq 0 ]; then
  echo No device yaml files found, aborting.
  exit 2
fi
shopt -u nullglob dotglob

# Dump stages in to output file
cat >>downstream-pipelines.yml <<EOL
stages:
  - lint
  - build
  - deploy

include:
  - .gitlab-ci/default.yml
EOL

# Loop over yaml files in configs folder
for FILE in "${files[@]}"
do
  echo $file
  # Trim down to just the file name
  NAME=`echo $FILE | cut -d\/ -f2 | cut -d\. -f1`
  # Append to file
cat >>downstream-pipelines.yml <<EOL

lint-$NAME:
  stage: lint
  tags:
    - ha-esphome-lint
  variables:
    FILE: $NAME
  script:
    - esphome config devices/${NAME}.yaml
  rules:
    - changes:
      - templates/**/*
      - devices/${NAME}.yaml
      - .gitlab-ci/default.yml
    - if: \$FORCE_CI == "" || \$FORCE_CI == "false"
      when: never
    - if: \$FORCE_CI == "true"

build-$NAME:
  stage: build
  tags:
    - ha-esphome-build
  variables:
    FILE: $NAME
  script:
    - esphome compile devices/${NAME}.yaml
  artifacts:
    paths:
      - devices/.esphome/build/${NAME}/.pioenvs/${NAME}/firmware.bin
    expire_in: 1 week
    public: false
  rules:
    - changes:
      - templates/**/*
      - devices/${NAME}.yaml
      - .gitlab-ci/default.yml
    - if: \$FORCE_CI == "" || \$FORCE_CI == "false"
      when: never
    - if: \$FORCE_CI == "true"

deploy-$NAME:
  stage: deploy
  tags:
    - ha-esphome-deploy
  variables:
    FILE: $NAME
  dependencies:
    - build-$NAME
  script:
    - esphome upload devices/${NAME}.yaml
  when: manual
  rules:
    - if: \$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH
      changes:
        - templates/**/*
        - devices/${NAME}.yaml
        - .gitlab-ci/default.yml
    - if: \$FORCE_CI == "" || \$FORCE_CI == "false"
      when: never
    - if: \$FORCE_CI == "true"
EOL

done
